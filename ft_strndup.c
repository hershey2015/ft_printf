/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 13:41:29 by coleksii          #+#    #+#             */
/*   Updated: 2017/04/06 17:00:21 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*ft_strndup(const char *s1, size_t n)
{
	char	*str1;
	size_t	i;

	i = 0;
	str1 = malloc(sizeof(char) * n + 1);
	if (!str1)
		return (0);
	while (n > 0)
	{
		str1[i] = s1[i];
		i++;
		n--;
	}
	str1[i] = '\0';
	return (str1);
}

char	*ft_strdup(char *s1)
{
	char	*str1;
	int		i;

	i = 0;
	str1 = malloc(sizeof(char) * ft_strlen(s1) + 1);
	if (!str1)
		return (0);
	while (s1[i] != '\0')
	{
		str1[i] = s1[i];
		i++;
	}
	str1[i] = '\0';
	return (str1);
}
