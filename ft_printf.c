/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/24 17:51:16 by coleksii          #+#    #+#             */
/*   Updated: 2017/04/06 16:30:12 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_printf(const char *format, ...)
{
	va_list argptr;
	int		i;
	int		l;
	t_plist	*lst;

	va_start(argptr, format);
	lst = (t_plist *)malloc(sizeof(t_plist));
	i = -1;
	l = 0;
	while (format[++i] != '\0')
	{
		if (format[i] == '%')
		{
			i = correct((char *)format, i, lst, &argptr);
			if (lst->type == 'n')
				*(va_arg(argptr, uintmax_t *)) = l;
			else
				l = l + parametres(lst, &argptr);
		}
		else if (++l)
			write(1, &format[i], 1);
	}
	va_end(argptr);
	free(lst);
	return (l);
}
