/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parametres.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coleksii <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/22 13:02:29 by coleksii          #+#    #+#             */
/*   Updated: 2017/04/10 15:11:36 by coleksii         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	function(t_plist *lst, va_list *argptr, int i)
{
	t_p	farg[15];

	farg[0] = decemical;
	farg[1] = decemical;
	farg[2] = decemical;
	farg[3] = ft_base_o;
	farg[4] = u_decemical;
	farg[5] = ft_base;
	farg[6] = ft_base;
	farg[7] = fft_putchar;
	farg[8] = fft_putchar;
	farg[9] = fft_putstr;
	farg[10] = u_decemical;
	farg[11] = fft_putstr_s;
	farg[12] = ft_base;
	farg[13] = ft_base_o;
	farg[14] = ft_base_o;
	farg[i](lst, argptr);
}

int		parametres(t_plist *lst, va_list *argptr)
{
	int		i;
	char	*s;

	s = "dDiouxXcCsUSpOb";
	i = 0;
	if (lst->size > 2 && (lst->type == 'c' || lst->type == 's'))
		lst->type -= 32;
	if (lst->prec != -2 && (lst->type != 'c' && lst->type != 'C' &&
				lst->type != 's' && lst->type != 'S' && lst->type != 't'))
		lst->nul = 0;
	if (lst->type == 'h' && (lst->str = (char *)malloc(1)))
		lst->str[0] = '\0';
	else if (lst->type != 't')
	{
		while (s[i] != '\0')
		{
			if (lst->type == s[i])
				break ;
			i++;
		}
		function(lst, argptr, i);
	}
	i = printable(lst);
	return (i);
}
