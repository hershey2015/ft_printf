# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: skholodn <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/11/28 12:29:36 by skholodn          #+#    #+#              #
#    Updated: 2017/04/07 17:08:50 by coleksii         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = @gcc

NAME = libftprintf.a

OBJ = ft_printf.o correct.o parametres.o decemical.o lst.o ft_putstr.o \
	  ft_itoa.o ft_putchar.o itoa_base.o u_itoa.o fft.o printable.o \
	  ft_strlen.o ft_strndup.o correct2.o bites.o

CFLAGS = -Wall -Wextra -Werror

all: $(NAME)

$(NAME): $(OBJ)
	@ar rc $(NAME) $(OBJ)
	@ranlib $(NAME)
	@echo "\033[32;1m<<lib done>>"
clean:
	@rm -f $(OBJ)
	
fclean: clean
	@rm -rf $(NAME)
	@echo "\033[32;1m<<delete o>>"

re: fclean all
	@echo "\033[32;1m<<re succes>>"
